#! /bin/bash
echo "Provisioning virtual machine..."

# Update Ubuntu 12.04 32-bit
echo "Updating apt-get repositories"
sudo apt-get update -y

# Install Apache
echo "Installing Apache"
sudo apt-get install apache2 -y

# Install MySQL & required installation dependencies
echo "Installing MySQL"
sudo apt-get install debconf-utils -y
debconf-set-selections <<< "mysql-server mysql-server/root_password password rootpw"
debconf-set-selections <<< "mysql-server mysql-server/root_password_again password rootpw"
sudo apt-get install mysql-server -y

# Setup MySQL database and users
# Privileges need to be re-granted and flushed after provisioning
echo -e "Creating mysql user drupal, database drupal, and importing database"
mysql -uroot -prootpw -e "CREATE DATABASE drupal;
USE drupal;
SOURCE /var/www/html/drupal/setup/dump.sql;
CREATE USER 'drupal'@'localhost' IDENTIFIED BY '/drupalpass/';
GRANT ALL PRIVILEGES ON drupal.* TO 'drupal'@'localhost' IDENTIFIED BY '/drupalpass/';
FLUSH PRIVILEGES;"

# Change MySQL bind address to allow connections from anywhere
sed -i "s/bind-address.*/bind-address = 0.0.0.0/" /etc/mysql/my.cnf

# Restart the SQL service
# Service needs to be restarted after provisioning
sudo service msyql restart

# Update PHP Repository
echo "Installing PHP"
apt-get install python-software-properties build-essential -y
add-apt-repository ppa:ondrej/php5 -y
apt-get update

# Install PHP
apt-get install php5-common php5-dev libapache2-mod-php5 php5-cli php5-fpm -y
apt-get install curl php5-curl php5-gd php5-mcrypt php5-mysql -y

# Enable apache mods
echo "Enabling mod-rewrite"
a2enmod rewrite
sed -i "s/AllowOverride None/AllowOverride All/g" /etc/apache2/apache2.conf
sed -i "s/error_reporting = .*/error_reporting = E_ALL/" /etc/php5/apache2/php.ini
sed -i "s/display_errors = .*/display_errors = On/" /etc/php5/apache2/php.ini

# Create vhost file
cp /var/www/html/drupal/setup/drupal_vhost /etc/apache2/sites-available/www.drupal.npvm.conf
sudo a2ensite www.drupal.npvm
sudo service apache2 restart

# Install Composer
echo "Installing Composer"
curl -sS https://getcomposer.org/installer | php
sudo mv composer.phar /usr/local/bin/composer
sed -i '1i export PATH="/home/vagrant/.composer/vendor/bin:$PATH"' /home/vagrant/.bashrc

# Install Drush
# This needs to be re-done after provisioning
echo "Installing Drush"
/usr/local/bin/composer global require drush/drush:6.*
touch /home/vagrant/.bash_profile
ln -s /home/vagrant/.composer/vendor/drush/drush/drush /usr/local/bin/drush
echo 'export PATH="/home/vagrant/.composer/vendor/drush/drush/drush:$PATH"' >> /home/vagrant/.bash_profile
